from lispy.tokens import Literal, Procedure

UNEXPECTED_EOF = ValueError("Unexpected EOF")


def tokenize(program: str) -> list:
    vals = program.replace("(", " ( ").replace(")", " ) ").split()
    parsed = _create_list(vals, 0)
    if not parsed:
        raise UNEXPECTED_EOF
    return parsed[0]


def _create_list(vals, count):
    parsed = []
    while vals:
        char = vals.pop(0)
        if char == "(":
            parsed.append(_create_list(vals, count + 1))
        elif char == ")":
            if count <= 0:
                raise UNEXPECTED_EOF
            return parsed
        elif not parsed:
            parsed.append(Procedure(char))
        else:
            parsed.append(Literal(char))

    return parsed


def parse_literal(value):
    v = value
    try:
        v = float(v)
        if v.is_integer():
            v = int(v)
    except ValueError:
        pass
    return v
