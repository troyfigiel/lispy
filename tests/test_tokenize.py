import pytest

from lispy.tokenize import parse_literal, tokenize
from lispy.tokens import Literal, Procedure


@pytest.mark.parametrize(
    ("input_program", "expected_tokens"),
    (
        (
            "x",
            Literal("x"),
        ),
        (
            "(f 1)",
            [Procedure("f"), Literal("1")],
        ),
        (
            "(f 1 2)",
            [Procedure("f"), Literal("1"), Literal("2")],
        ),
        (
            "(f (g 1))",
            [Procedure("f"), [Procedure("g"), Literal("1")]],
        ),
        (
            "(f (g 1) (h 2))",
            [
                Procedure("f"),
                [Procedure("g"), Literal("1")],
                [Procedure("h"), Literal("2")],
            ],
        ),
    ),
)
def test_tokenize_for_valid_programs(input_program, expected_tokens):
    actual_tokens = tokenize(input_program)

    assert actual_tokens == expected_tokens


@pytest.mark.parametrize(
    "input_program",
    (
        "",
        ")",
        "1)",
        "(f 1))",
        # "(f 1) (g 2)",
    ),
)
def test_tokenize_for_invalid_programs(input_program):
    with pytest.raises(ValueError):
        tokenize(input_program)


@pytest.mark.parametrize(
    ("input_literal", "expected_value"),
    (
        (Literal("hello"), "hello"),
        (Literal("3.2"), 3.2),
        (Literal("1"), 1),
    ),
)
def test_parse_literal(input_literal, expected_value):
    actual_value = parse_literal(input_literal)

    assert actual_value == expected_value
