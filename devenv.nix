{ config, pkgs, ... }:

{
  languages.python = {
    enable = true;
    package =
      pkgs.python310.withPackages (ps: with ps; [ coverage hypothesis pytest ]);
  };

  packages =
    (with pkgs; [ mutmut refurb python3Packages.jedi-language-server ]);

  pre-commit.hooks = {
    black.enable = true;
    mypy.enable = true;
    nixfmt.enable = true;
    ruff.enable = true;
  };

  pre-commit.settings = { };
}
